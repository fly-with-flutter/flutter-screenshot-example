import 'dart:io';
import 'dart:typed_data';
import 'dart:ui' as ui;
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:path_provider/path_provider.dart';

void main() {
  runApp(MainApp());
}

class MainApp extends StatelessWidget {
  MainApp({super.key});
  final GlobalKey globalKey = GlobalKey();
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          body: Center(
            child: Column( mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
            RepaintBoundary( key: globalKey,
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: Colors.cyan,
                ),
                padding: const EdgeInsets.all(8),
                child: SizedBox(width: 90,height: 45,  child: const Text('Hello I\'m ScreenShot')),
              ),
            ),SizedBox(height: 15,),
            ElevatedButton(
                onPressed: () async {
                  final image = await captureWidget();
                  final dir = await getExternalStorageDirectories();
                  final file = File('${dir!.first.path}example.png');
                  await file.writeAsBytes(image);
                },
                child: const Text('Take ScreenShot'))
        ],
      ),
          )),
    );
  }

  Future<Uint8List> captureWidget() async {
    RenderRepaintBoundary boundary = globalKey.currentContext!.findRenderObject() as RenderRepaintBoundary;

    final ui.Image image = await boundary.toImage(pixelRatio: 16);

    final ByteData? byteData = await image.toByteData(format: ui.ImageByteFormat.png);

    final Uint8List pngBytes = byteData!.buffer.asUint8List();

    return pngBytes;
  }
}
