# Fly With Flutter
## Take ScreenShot From Widget example
This example explains how to take an image of a widget in app that can be saved or shared from the app

## instructions
1- Wrap the widget you want to capture in a RepaintBoundary widget:
```
RepaintBoundary(
  child: YourWidget(),
)
```
2- Capture the screenshot:
```
GlobalKey imageKey = GlobalKey();

// Capture the screenshot
Future<Uint8List?> captureScreenshot() async {
  RenderRepaintBoundary boundary = imageKey.currentContext!.findRenderObject() as RenderRepaintBoundary;
  ui.Image image = await boundary.toImage(pixelRatio: 1.0);
  ByteData? byteData = await image.toByteData(format: ui.ImageByteFormat.png);
  return byteData?.buffer.asUint8List();
}
```
3- Display or save the screenshot: Once you have the screenshot as a Uint8List, you can display it or save it as per your requirements.

4-That's it! By using RepaintBoundary and RenderRepaintBoundary classes, you can capture a screenshot of a specific widget in your Flutter app without relying on external packages.

----------

> Remember to handle any necessary permissions or file operations if you want to save the screenshot to local storage.

----------
**Don't Mess Up And Follow Me For More**

[LinkedIn](https://www.linkedin.com/in/omar-kaialy-988531210)

[Telegram](https://t.me/omarlord1221)

[Gitlab](https://www.gitlab.com/omarlord1221)
